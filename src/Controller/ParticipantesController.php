<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Participantes;
use App\Form\ParticipantesFormType;
use App\Entity\Municipios;


use Symfony\Component\HttpFoundation\Request;  //componente para recibir request de formularios
//use Doctrine\ORM\EntityManagerInterface;


use Doctrine\ODM\PHPCR\Query\QueryException;


class ParticipantesController extends AbstractController
{
    /**
     * @Route("/participantes", name="participantes")
     */
    public function index(Request $request): Response
    {


    	$participantes=new Participantes();
    	$form=$this->createForm(ParticipantesFormType::class,$participantes); //crear instancia formulario

    	$form->handleRequest($request);
    	if($form->isSubmitted() && $form->isValid()){  //si el formulario se envio y tiene la informacion correcta
    		$em= $this->getDoctrine()->getManager();  //administrador de entidades
    		$em->persist($participantes);  //guarda valores cache
    		$em->flush();  //inserta en bd 
    		$this->addFlash('exito',Participantes::REGISTRO_EXITOSO);  //type , message
    		return($this->redirectToRoute('participantes'));  //route
    	}


        return $this->render('participantes/index.html.twig', [
            'controller_name' => 'ParticipantesController',
             'formulario'=>$form->createView()  //creamos el html del formulario para retornar a la vista
        ]);
    }

    /**
     * @Route("/participantes/validaDocumento/{documento}", name="ValidaDocumento")
     */
    public function validarDocumento ($documento): Response{ 
		$em= $this->getDoctrine()->getManager();  //administrador de entidades
		$existe=$em->getRepository(Participantes::class)->findOneBy(['documento' => $documento]);
		$existe = (array) $existe;

		$retorno=array();
		$retorno['exito']=1;

		if(count($existe)){
			$retorno['exito']=0;
		}

		//retorno json
	    $response = new Response(json_encode($retorno));
	    $response->headers->set('Content-Type', 'application/json');

	    return $response;		
    }

    /**
     * @Route("/participantes/AutocompletarCiudad/{ciudad}", name="AutocompletarCiudad")
     */
    public function AutocompletarCiudad ($ciudad): Response{ 
		$em= $this->getDoctrine()->getManager();  //administrador de entidades
		$sql=$em->getRepository(Municipios::class)->createQueryBuilder('o')
		   ->where('LOWER(o.municipio) LIKE :municipio')
		   ->setParameter('municipio', '%'.strtolower($ciudad).'%')
		   ->getQuery();

		$consulta=$sql->getResult();

		$arr_municipios=array();
		for ($i=0; $i < count($consulta); $i++) { 
			$arr_municipios[]=array(
				'id'=>$consulta[$i]->getId(),
				'municipio'=>$consulta[$i]->getMunicipio()
			);
		}

		$retorno=array();
		$retorno['exito']=1;
		$retorno['municipios']=$arr_municipios;


		//retorno json
	    $response = new Response(json_encode($retorno));
	    $response->headers->set('Content-Type', 'application/json');

	    return $response;		
    }


}

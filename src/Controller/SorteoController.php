<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Participantes;
use App\Entity\Sorteo;




class SorteoController extends AbstractController
{
    /**
     * @Route("/sorteo", name="sorteo")
     */
    public function index(): Response
    {

        return $this->render('sorteo/index.html.twig', [
            'controller_name' => 'SorteoController',
        ]);
    }


    /**
     * @Route("/sorteo/ganadores", name="sorteoGanandores")
     */
    public function ganadoresSorteo(){

     	$sql="
    		SELECT 
    			a.nombre,
    			a.documento 
    		FROM	
    			App\Entity\Participantes a,
    			App\Entity\Sorteo b 
    		WHERE
    			a.documento=b.documento
    	";
		$em= $this->getDoctrine()->getManager();  //administrador de entidades
	    $consulta = $em->createQuery($sql)->getResult();
	    print_r($consulta); die();
	    /*
		$arr_ganadores=array();
		for ($i=0; $i < count($consulta); $i++) { 
			$arr_ganadores[]=array(
				'id'=>$consulta[$i]->getId(),
				'municipio'=>$consulta[$i]->getMunicipio()
			);
		}
		*/
		$retorno=array();
		$retorno['exito']=1;
		$retorno['ganadores']=$arr_ganadores;  

    }


    /**
     * @Route("/sorteo/registrarGanador", name="registrarGanador")
     */
    public function registrarGanador(){
	  
      	$sql="
    		SELECT 
    			a.documento 
    		FROM	
    			App\Entity\Participantes AS a 
    			LEFT JOIN 
    				App\Entity\Sorteo AS b 
    			ON 
    				a.documento=b.documento_id
    		WHERE
    			b.documento_id IS NULL
    	";
		$em= $this->getDoctrine()->getManager();  //administrador de entidades
	    $consulta = $em->createQuery($sql)->getResult();
	    print_r($consulta); die();

	     	

	    /*
    	$sorteo=new Sorteo();
    	$form=$this->createForm(ParticipantesFormType::class,$sorteo); //crear instancia formulario

   		$em= $this->getDoctrine()->getManager();  //administrador de entidades
   		$em->persist($participantes);  //guarda valores cache
   		$em->flush();  //inserta en bd 
    	*/



    }

}

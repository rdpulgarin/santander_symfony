<?php

namespace App\Entity;

use App\Repository\SorteoRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\Participantes;

/**
 * @ORM\Entity(repositoryClass=SorteoRepository::class)
 */
class Sorteo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

   /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Participantes", inversedBy="documento")
     */
    private $documento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDocumento(): ?int
    {
        return $this->documento;
    }

    public function setDocumento(int $documento): self
    {
        $this->documento = $documento;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\ParticipantesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParticipantesRepository::class)
 */
class Participantes
{

    const REGISTRO_EXITOSO = 'Se ha registrado exitosamente al concurso';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sorteo", mappedBy="documento")
     */    
    private $documento;

    /**
     * @ORM\Column(type="integer")
     */
    private $ciudad;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDocumento(): ?int
    {
        return $this->documento;
    }

    public function setDocumento(int $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getCiudad(): ?int
    {
        return $this->ciudad;
    }

    public function setCiudad(int $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }
}
